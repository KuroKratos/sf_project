// any CSS you import will output into a single css file (admin.css in this case)
import './styles/admin.scss';

import './js/trix-base64-attachements'

import './js/sortable-collection-field';

/**
 * Handle tab visibility on page load
 */

document.addEventListener('DOMContentLoaded', e => {
    const pageId = document.body.getAttribute('id');
    const tabButtons = document.querySelectorAll('[data-bs-toggle="tab"]');
    let activeTabNumber = localStorage.getItem("admin/active-tab/"+pageId);

    tabButtons.forEach((tb, key) => {
        tb.addEventListener('click', e => {
            localStorage.setItem("admin/active-tab/"+pageId, `${key}`);
        });
    });

    if(activeTabNumber !== null) {
        activeTabNumber = parseInt(activeTabNumber);

        if (activeTabNumber < tabButtons.length) {
            const tabInst = bootstrap.Tab.getOrCreateInstance(tabButtons[activeTabNumber]);
            tabInst.show();
        }
    }
});