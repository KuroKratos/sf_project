/**
 * Listens for trix editor attachments and convert them to data-uri to store them in directly in the document
 */

{
    addEventListener("trix-attachment-add", function(event) {
        if (event.attachment.file) {
            uploadFileAttachment(event.attachment)
        }
    });

    function uploadFileAttachment(attachment) {
        const reader = new FileReader();
        reader.onloadend = () => {
            const base64String = reader.result;
            const attributes = {
                url: base64String,
            }
            attachment.setAttributes(attributes);
        };
        reader.readAsDataURL(attachment.file);
    }
}