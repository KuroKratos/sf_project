FROM php:8.2-cli-buster

RUN mkdir /source

RUN apt-get update && apt-get install -y \
        git \
        libzip-dev \
        libicu-dev \
        zlib1g-dev \
        libxml2-dev
RUN docker-php-ext-install -j$(nproc) zip intl xml
RUN php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=/usr/local/bin --filename=composer \
    && chmod +x /usr/local/bin/composer

# Avoid the prompt asking to validate remote server fingerprint when using git over ssh
RUN sed -i "s/^#   StrictHostKeyChecking ask.*$/    StrictHostKeyChecking no/g" /etc/ssh/ssh_config

RUN echo "memory_limit = 2G" > $PHP_INI_DIR/conf.d/memory.ini

WORKDIR /source
CMD ["composer", "install", "--no-progress", "--no-interaction", "--ignore-platform-reqs"]
