PHP build container
=======================

This container is intended to pull PHP vendors of a project with Composer and to dump FOS JsRouting.

## Usage
```bash
docker build -t pantogral/php-build $(pwd)/docker/php-build

# By default it will install composer.json in /source
docker run --rm -it -v $(pwd)/source:/source pantogral/php-build

# Update a single package
docker run --rm -it -v $(pwd)/source:/source pantogral/php-build composer --ignore-platform-reqs update "pantogral/background-jobs"

# Dump FOS JsRouting for dev env
docker run --rm -v $(pwd)/source:/source -v $(pwd)/docker/parameters.dev.yml:/source/app/config/parameters.yml pantogral/composer bash -c "php bin/console cache:clear --env=dev; php bin/console --env=dev fos:js-routing:dump --format=json --target=assets/js/fos_js_routes.json"
```

## Caution
When installing the `--ignore-platform-reqs` is used in order to avoid warning and errors due to php extensions missing.
Thus make sure to include PHP's version in `composer.json`.

```json
{
"config": {
        "platform": {
            "php": "7.2.34"
        }
    }
}
```
