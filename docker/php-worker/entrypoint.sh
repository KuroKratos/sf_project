#!/bin/sh
set -e

[ -d "/data/var/log" ] && chown -R www-data:www-data /data/var/log
[ -d "/data/var/cache" ] && chown -R www-data:www-data /data/var/cache && rm -rf /data/var/cache/{dev,prod}
[ -d "/data/var/var/documents" ] && chown -R www-data:www-data /data/var/var/documents
[ -d "/data/var/public/documents" ] && chown -R www-data:www-data /data/var/public/documents
[ -d "/data/var/public/documents" ] && \
  mkdir -p "/data/var/public/documents/event-covers/" && \
  mkdir -p "/data/var/public/documents/qrcodes/" && \
  chown -R www-data:www-data /data/var/public/documents/*

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php "$@"
fi

exec "$@"
