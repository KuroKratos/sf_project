#!/bin/sh
set -e

[ -d "/var/www/html/var/log" ] && chown -R www-data:www-data /var/www/html/var/log
[ -d "/var/www/html/var/cache" ] && chown -R www-data:www-data /var/www/html/var/cache && rm -rf /var/www/html/var/cache/{prod,dev}
[ -d "/var/www/html/var/documents" ] && chown -R www-data:www-data /var/www/html/var/documents
[ -d "/var/www/html/public/documents" ] && chown -R www-data:www-data /var/www/html/public/documents
[ -d "/var/www/html/public/documents" ] && \
  mkdir -p "/var/www/html/public/documents/event-covers/" && \
  mkdir -p "/var/www/html/public/documents/qrcodes/" && \
  chown -R www-data:www-data /var/www/html/public/documents/*

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

exec "$@"
