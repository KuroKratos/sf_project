map $http_user_agent $sf_app_env {
    default dev;
    ~selenium_test$ test;
}
map $http_user_agent$sf_app_env $sf_test_session_storage_class {
    default '';
    ~selenium_testtest$ "Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage";
}

server {
    listen 80;

    root /usr/share/nginx/html;

    include fastcgi_params;

    # When you are using symlinks to link the document root to the
    # current version of your application, you should pass the real
    # application path instead of the path to the symlink to PHP
    # FPM.
    # Otherwise, PHP's OPcache may not properly detect changes to
    # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
    # for more information).
    fastcgi_param APP_ENV $sf_app_env;
    fastcgi_param TEST_SESSION_STORAGE_CLASS $sf_test_session_storage_class;
    fastcgi_param DOCUMENT_ROOT /var/www/html/public/;
    fastcgi_param SCRIPT_FILENAME /var/www/html/public/$fastcgi_script_name;
    fastcgi_read_timeout 120s;

    client_max_body_size 50m;
    client_body_timeout 300s;

    location / {
        # try to serve file directly, fallback to app.php
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass app:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
      return 404;
    }

    error_log /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
}
