server_tokens off;

map $http_x_forwarded_ssl $proxied_scheme {
        off http;
        on https;
}

map $http_x_forwarded_ssl $http_x_forwarded_proto {
        off http;
        on https;
}

map $http_x_forwarded_ssl $proxied_port {
        off 80;
        on 443;
}

server {
    listen 80;

    root /usr/share/nginx/html;

    include fastcgi_params;

    # When you are using symlinks to link the document root to the
    # current version of your application, you should pass the real
    # application path instead of the path to the symlink to PHP
    # FPM.
    # Otherwise, PHP's OPcache may not properly detect changes to
    # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
    # for more information).
    fastcgi_param DOCUMENT_ROOT /var/www/html/public/;
    fastcgi_param SCRIPT_FILENAME /var/www/html/public/$fastcgi_script_name;

    # Un-comment those parameters to accept SSL gateway proxying (production)
    fastcgi_param  SERVER_PROTOCOL    $proxied_scheme;
    fastcgi_param  REMOTE_ADDR        $http_x_real_ip;
    fastcgi_param  REMOTE_PORT        $proxied_port;
    fastcgi_param  HTTPS              $http_x_forwarded_ssl;

    client_max_body_size 50m;
    client_body_timeout 300s;

    location / {
        # try to serve file directly, fallback to app.php
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass app:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        internal;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
      return 404;
    }
    error_log /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
}
