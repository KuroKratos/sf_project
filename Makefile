.PHONY: *

mkfile_path := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))
yarn_image := "node:19.6.0-bullseye"

prepare-docker:
	DOCKER_BUILDKIT=1 docker build -q $(mkfile_path)/docker/php-build -t sf_project/php-build
	DOCKER_BUILDKIT=1 docker build $(mkfile_path)/docker/php-cs-fixer -t sf_project/php-cs-fixer

js-resources:
	docker run --rm -v $(mkfile_path)/source:/app -w /app $(yarn_image) yarn install

fix-perms:
	docker run --rm -v $(mkfile_path)/source:/app -w /app $(yarn_image) chown -R 1000:1000 /app/vendor
	docker run --rm -v $(mkfile_path)/source:/app -w /app $(yarn_image) chown -R 1000:1000 /app/public
	docker run --rm -v $(mkfile_path)/source:/app -w /app $(yarn_image) chown -R 1000:1000 /app/var
	#docker run --rm -v $(mkfile_path)/source:/app -w /app $(yarn_image) chown -R 1000:1000 /app/assets/js/fos_js_routes.json

#yarn-add:
#	docker run --rm -v $(mkfile_path)/source:/app -w /app $(yarn_image) yarn add bootstrap-icons --dev

encore-dev: js-resources
	docker run --rm -v $(mkfile_path)/source:/app -w /app $(yarn_image) yarn encore dev

encore-watch: js-resources
	docker run --rm -v $(mkfile_path)/source:/app -w /app $(yarn_image) yarn encore dev --watch

encore-prod: js-resources
	docker run --rm -v $(mkfile_path)/source:/app -w /app $(yarn_image) yarn encore production

composer-dev: prepare-docker
	docker run --rm \
		-v $(mkfile_path)/source:/source \
		-v $(mkfile_path)/docker/.env.dev:/source/.env \
		-v $(mkfile_path)/.composer:/tmp/composer \
		-e COMPOSER_CACHE_DIR=/tmp/composer \
		sf_project/php-build composer update --no-progress --no-interaction --ignore-platform-reqs

#composer-recipes: prepare-docker
#	docker run --rm \
#		-v $(mkfile_path)/source:/source \
#		-v $(mkfile_path)/docker/.env.dev:/source/.env \
#		-v $(mkfile_path)/.composer:/tmp/composer \
#		-e COMPOSER_CACHE_DIR=/tmp/composer \
#		sf_project/php-build composer recipes:install symfony/web-profiler-bundle --force -v

#require: prepare-docker
#	docker run --rm \
#		-v $(mkfile_path)/source:/source \
#		-v $(mkfile_path)/docker/.env.dev:/source/.env \
#		-v $(mkfile_path)/.composer:/tmp/composer \
#		-e COMPOSER_CACHE_DIR=/tmp/composer \
#		sf_project/php-build composer require twig/intl-extra --no-progress --no-interaction --ignore-platform-reqs

composer-prod: prepare-docker
	docker run --rm \
		-v $(mkfile_path)/source:/source \
		-v $(mkfile_path)/docker/.env.prod:/source/.env \
		-v $(mkfile_path)/.composer:/tmp/composer \
		-e COMPOSER_CACHE_DIR=/tmp/composer \
		sf_project/php-build composer install --no-dev --no-progress --no-interaction --ignore-platform-reqs

#fos-js-routes-dev: prepare-docker
#	docker run --rm \
#		-v $(mkfile_path)/source:/source \
#		-v $(mkfile_path)/docker/.env.dev:/source/.env \
#		sf_project/php-build \
#		bash -c "rm -rf var/cache/dev; php bin/console --env=dev fos:js-routing:dump --format=json --target=assets/js/fos_js_routes.json"
#
#fos-js-routes-prod: prepare-docker
#	docker run --rm \
#		-v $(mkfile_path)/source:/source \
#		-v $(mkfile_path)/docker/.env.prod:/source/.env \
#		sf_project/php-build \
#		bash -c "rm -rf var/cache/prod; php bin/console --env=prod fos:js-routing:dump --format=json --target=assets/js/fos_js_routes.json"

#dev-fe: fos-js-routes-dev encore-dev fix-perms
dev-fe: encore-dev fix-perms
#dev: composer-dev fos-js-routes-dev encore-dev fix-perms
dev: composer-dev encore-dev fix-perms

#prod-fe: fos-js-routes-prod encore-prod fix-perms
prod-fe: encore-prod fix-perms
#prod: composer-prod fos-js-routes-prod encore-prod fix-perms
prod: composer-prod encore-prod fix-perms

php-cs-fixer: prepare-docker
	docker run --rm \
		-v $(mkfile_path)/source:/source \
		maddox/php-cs-fixer php-cs-fixer fix src