#!/usr/bin/env bash
set -ueo pipefail

# shellcheck disable=SC2164
if [ -z "$1" ]
  then
    echo "Missing tag argument"
    exit
fi
if git rev-parse "$1" >/dev/null 2>&1
  then
    echo "Tag already exists"
    exit
fi
echo "RELEASE v$1 START"
git fetch --all && git pull origin master && git merge origin/dev --commit --no-edit
rm source/vendor/* -rf
make prod
git add -f source/public/build/ source/public/bundles source/vendor/ source/assets/js/fos_js_routes.json
git commit -m "[RELEASE] v$1"
git tag "$1"
echo "RELEASE v$1 DONE"
